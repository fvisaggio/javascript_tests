function NameParser() {
   parsedName = {
     firstName: "N/A",
     middleName: "N/A",
     lastName:  "N/A",
     originalString: "Null and Void"
  };
}
NameParser.prototype.parse_name = function(originalString) {
  parsedName.originalString = originalString;
  var partsOfString = parsedName.originalString.split(" ");
  if (partsOfString.length ==1) {
    parsedName.firstName = partsOfString[0];
  }
  else if (partsOfString.length == 2) {
    parsedName.firstName = partsOfString[0];
    parsedName.lastName = partsOfString[1];
  }
  else if (partsOfString.length == 3) {
    parsedName.firstName = partsOfString[0];
    parsedName.middleName = partsOfString[1];
    parsedName.lastName = partsOfString[2];
  }
  else {
    throw new Error("name to be parsed has too many spaces");
  }
  return parsedName;
};


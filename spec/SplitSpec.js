  // nameHelper = function(name_array) {
  //   if (name_array.length == 3){
  //     expectedParsedName.firstName = name_array[0];
  //     expectedParsedName.middleName = name_array[1];
  //     expectedParsedName.lastName = name_array[2];
  //   }
  //   else if (name_array.length == 2){
  //     expectedParsedName.firstName = name_array[0];
  //     expectedParsedName.lastName = name_array[1];
  //   }
  //   else {
  //     expectedParsedName.firstName = name_array[0];
  //   }
  // });
  // ********* was in initial 
   //   parser = new NameParser();
   //  initialexpectedParsedName = {
   //                         firstName: "N/A",
   //                         middleName: "N/A",
   //                         lastName:  "N/A"
   //                      };


describe("NameParser", function() {

  beforeEach(function() {
    parser = new NameParser();
  });

  it("should parse a single name as a firstName and set the other to N/A ", function() {
    parsedName=parser.parse_name("mueller");
    expect(parsedName.firstName).toEqual("mueller");
    expect(parsedName.middleName).toEqual("N/A");
    expect(parsedName.lastName).toEqual("N/A");
  });

  it("should keep the original string in the output hash", function() {
    parsedName=parser.parse_name("mueller 1");
    expect(parsedName.originalString).toEqual("mueller 1");
  });

  it("should parse names with one space (firstName, lastName) as a hash of key value pairs", function() {
    parsedName=parser.parse_name("mike jordan");
    expect(parsedName.firstName).toEqual("mike");
    expect(parsedName.lastName).toEqual("jordan");
  });

  it("should set the middle name to N/A if there is none", function() {
    parsedName=parser.parse_name("mike jordan");
    expect(parsedName.middleName).toEqual("N/A");
  });

  it("should parse names with Uppercase Letters", function() {
    parsedName=parser.parse_name("Alan Greenspan");
    expect("Alan").toEqual(parsedName.firstName);
    expect("Greenspan").toEqual(parsedName.lastName);
  });

  it("should parse names with middle names", function() {
    parsedName=parser.parse_name("Brett Lorenzo Favre");
    expect("Brett").toEqual(parsedName.firstName);
    expect("Lorenzo").toEqual(parsedName.middleName);
    expect("Favre").toEqual(parsedName.lastName);
  });

  it("should parse names with Hyphens", function() {
    parsedName=parser.parse_name("kabeer gbaja-biamila");
    expect("kabeer").toEqual(parsedName.firstName);
    expect("N/A").toEqual(parsedName.middleName);
    expect("gbaja-biamila").toEqual(parsedName.lastName);
  });

  it("should error names with excessive spaces ie 2 word last names ", function() {
    expect( function () {
      parsedName=parser.parse_name("Jean-Claude aaron Van Dam");
    }).toThrow(new Error("name to be parsed has too many spaces"));
  });

});